import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import client from "./client";

const store = new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {
    getUserGists(store, { username }) {
      return new Promise((resolve, reject) => {
        client
          .get(`/users/${username}/gists`)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    getGistForks(store, { gist_id }) {
      return new Promise((resolve, reject) => {
        client
          .get(`/gists/${gist_id}/forks`, {
            params: {
              gist_id: gist_id,
              per_page: 3,
            },
          })
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
});

export default store;
